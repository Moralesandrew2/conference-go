import requests
import json
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
from django.http import JsonResponse


def get_picture_url(city, state):
    url = (
        "https://api.pexels.com/v1/search?query="
        + city
        + ","
        + state
        + "&per_page=1"
    )
    headers = {"Authorization": PEXELS_API_KEY}
    r = requests.get(url, headers=headers)
    search = r.json()
    img_url = search["photos"][0]["src"]["original"]
    img = {"picture_url": img_url}
    return img


def get_weather_data(city, state):
    url = (
        "http://api.openweathermap.org/geo/1.0/direct?q="
        + city
        + ","
        + state
        + ","
        + "US"
        + "&limit=1&appid="
        + OPEN_WEATHER_API_KEY
    )
    headers = {"Authorization": OPEN_WEATHER_API_KEY}
    r = requests.get(url, headers=headers)
    data = r.json()
    lat = data[0]["lat"]
    lon = data[0]["lon"]
    weather_url = (
        "https://api.openweathermap.org/data/2.5/weather?lat="
        + str(lat)
        + "&lon="
        + str(lon)
        + "&appid="
        + OPEN_WEATHER_API_KEY
        + "&units=imperial"
    )
    r_weather = requests.get(weather_url)
    weather_info = r_weather.json()
    weather_data = {
        "temperature": weather_info["main"]["temp"],
        "description": weather_info["weather"][0]["description"],
    }
    return weather_data
