import json
import pika
import django
import os
import sys
import time
from django.core.mail import send_mail
from pika.exceptions import AMQPConnectionError

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()


def process_approval(ch, method, properties, body):
    print("  Received %r" % body)
    presentation = json.loads(body)
    send_mail(
        "Your presentation has been accepted",
        f"{presentation['presenter_name']}, we're happy to tell you that your presentation {presentation['title']} has been accepted",
        "admin@conference.go",
        [f"{presentation['presenter_email']}"],
        fail_silently=False,
    )


def process_rejection(ch, method, properties, body):
    print("  Received %r" % body)
    presentation = json.loads(body)
    send_mail(
        "Your presentation has been rejected",
        f"{presentation['presenter_name']}, we regret to tell you that your presentation {presentation['title']} has been rejected",
        "admin@conference.go",
        [f"{presentation['presenter_email']}"],
        fail_silently=False,
    )


while True:
    try:
        parameters = pika.ConnectionParameters(host="rabbitmq")
        connection = pika.BlockingConnection(parameters)
        channel = connection.channel()
        channel.queue_declare(queue="presentation_rejections")
        channel.queue_declare(queue="presentation_approvals")
        channel.basic_consume(
            queue="presentation_rejections",
            on_message_callback=process_rejection,
            auto_ack=True,
        )
        channel.basic_consume(
            queue="presentation_approvals",
            on_message_callback=process_approval,
            auto_ack=True,
        )
        channel.start_consuming()
    except AMQPConnectionError:
        print("Could not connect to RabbitMQ")
        time.sleep(2.0)
